EG 44175 Ethical and Professional Issues in Engineering
=======================================================

This is the source code for the EG 44175 Ethical and Professional Issues in
Engineering (Summer 2018) [course
website](http://www3.nd.edu/~pbui/teaching/eg.44175.su18/).
